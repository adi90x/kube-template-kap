// Copyright © 2015 Victor Antonovich <victor@antonovich.me>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"
	"reflect"
	"regexp"
	"strings"
	"path"
	"sort"

	gotemplate "text/template"

	//"github.com/Masterminds/sprig"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	corev1 "k8s.io/api/core/v1"

)

const (
	DEFAULT_NAMESPACE = metav1.NamespaceDefault
	DEFAULT_SELECTOR  = ""
)

type Template struct {
	// Template descriptor from configuration
	desc *TemplateDescriptor

	// Template name (base file name)
	name string

	// Go template to render
	template *gotemplate.Template

	// Template last output (in case of successfully rendered template)
	lastOutput string
}

func newTemplate(cfg *Config, dm *DependencyManager, d *TemplateDescriptor) (*Template, error) {
	// Template name
	name := filepath.Base(d.Path)
	// Get last template output, if present
	o, err := ioutil.ReadFile(d.Output)
	if err != nil {
		o = nil
	}
	// Read template data
	data, err := ioutil.ReadFile(d.Path)
	if err != nil {
		return nil, err
	}
	s := string(data)
	// Create Go template from read data
	template, err := gotemplate.New(name).Delims(cfg.LeftDelimiter, cfg.RightDelimiter).Funcs(funcMap(dm)).Parse(s)
	if err != nil {
		return nil, err
	}
	// Create template
	return &Template{
		desc:       d,
		name:       name,
		template:   template,
		lastOutput: string(o),
	}, nil
}

func newTemplatesFromConfig(cfg *Config, dm *DependencyManager) ([]*Template, error) {
	templates := make([]*Template, 0, len(cfg.TemplateDescriptors))
	for _, d := range cfg.TemplateDescriptors {
		t, err := newTemplate(cfg, dm, d)
		if err != nil {
			return nil, err
		}
		templates = append(templates, t)
	}
	return templates, nil
}

func (t *Template) Process(dryRun bool) (bool, error) {
	if r, err := t.Render(); err == nil {
		if changed := !(r == t.lastOutput); changed {
			// Template output changed
			if !dryRun {
				if err := t.Write([]byte(r)); err != nil {
					// Can't write template output
					return false, err
				}
			}
			t.lastOutput = r
			return true, nil
		}
		// Template output not changed
		return false, nil
	} else {
		// Can't render template
		return false, err
	}
}

func (t *Template) Write(content []byte) error {
	dir := filepath.Dir(t.desc.Output)
	if _, err := os.Stat(t.desc.Output); os.IsNotExist(err) {
		// Output file does not exist, create intermediate dirs and write directly
		if err := os.MkdirAll(dir, 0755); err != nil {
			return err
		}
		// TODO file mode from config
		if err := ioutil.WriteFile(t.desc.Output, content, 0644); err != nil {
			return err
		}
	} else {
		// Output file exist, update atomically using temp file
		var f *os.File
		if f, err = ioutil.TempFile(dir, t.name); err != nil {
			return err
		}
		defer UnlinkQuietly(f.Name())
		// Write template output to temp file
		if _, err := f.Write(content); err != nil {
			return err
		}
		if err := f.Sync(); err != nil {
			return err
		}
		if err := f.Close(); err != nil {
			return err
		}
		// TODO file mode from config
		if err := os.Chmod(f.Name(), 0644); err != nil {
			return err
		}
		// Rename temp file to output file
		if err := os.Rename(f.Name(), t.desc.Output); err != nil {
			return err
		}
	}
	return nil
}

func (t *Template) Render() (string, error) {
	// Render template to buffer
	buf := new(bytes.Buffer)
	if err := t.template.Execute(buf, nil); err != nil {
		return "", err
	}

	return buf.String(), nil
}

func funcMap(dm *DependencyManager) gotemplate.FuncMap {
	f := gotemplate.FuncMap{
		// Legacy helper functions
		"toLower":   strings.ToLower,
		"toUpper":   strings.ToUpper,
		"toTitle":   strings.Title,
		"trimSpace": strings.TrimSpace,

		//Utility function
		"contains":  strings.Contains,
		"replace":   strings.Replace,
		"base":      path.Base,
		"dir":       path.Dir,
		"env":       os.Getenv,
		"timestamp": time.Now,
		"split":     strings.Split,
		"join":      strings.Join,

		//KAP Specific fonction ( copy from adi90x/rancher-gen-rap)
		"exists":           	exists,
		"groupByMulti":      	groupByMulti,
		"dict":              	dict,
		"trimSuffix":        	strings.TrimSuffix,
		"closest":           	arrayClosest,
		"first":             	arrayFirst,
		"last":              	arrayLast,
		"coalesce":          	coalesce,
		"trim":              	strings.TrimSpace,
		"dirList":           	dirList,
		"when":               when,
    "concatenateUnique":  concatenateUnique,
    "groupByMultiFilter": groupByMultiFilter,
    "getAllLabelValue":	  getAllLabelValue,
		"formatClean": 		    formatClean,
		"filterHost":         filterHost,
		"filterLabel":        filterLabel,
		"cell":               NewCell,
		"GetValue":           GetValue,
		"ExistValue":         ExistValue,
		"uniqueMap":          uniqueMap,
		"getPortsLen":        getPortsLen,
		"getFirstPort":       getFirstPort,
		"isOnline":           isOnline,
		"getIP":              getIP,
		"aggregateMap":       aggregateMap,
	}

	// Kubernetes objects functions
	for k, v := range kubeObjectsFuncMap(dm) {
		f[k] = v
	}

	// Sprig helper functions
	//for k, v := range sprig.FuncMap() {
	//	f[k] = v
	//}

	return f
}

// Parse template tag with max 1 argument - selector
func parseSelector(s ...string) (string, error) {
	selector := DEFAULT_SELECTOR
	switch len(s) {
	case 0:
		break
	case 1:
		selector = s[0]
	default:
		return "", fmt.Errorf("expected max 1 argument, got %d", len(s))
	}
	return selector, nil
}

// Parse template tag with max 2 arguments - selector and namespace (in given order)
func parseNamespaceSelector(s ...string) (string, string, error) {
	namespace, selector := DEFAULT_NAMESPACE, DEFAULT_SELECTOR
	switch len(s) {
	case 0:
		break
	case 1:
		selector = s[0]
	case 2:
		selector = s[0]
		namespace = s[1]
	default:
		return "", "", fmt.Errorf("expected max 2 arguments, got %d", len(s))
	}
	return namespace, selector, nil
}

// Adding Template Function for KAP
//KAP : exists ( from adi90x/rancher-gen-rap)
func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}
//KAP : dict ( from adi90x/rancher-gen-rap)
func dict(values ...interface{}) (map[string]interface{}, error) {
	if len(values)%2 != 0 {
		return nil, fmt.Errorf("invalid dict call")
	}
	dict := make(map[string]interface{}, len(values)/2)
	for i := 0; i < len(values); i += 2 {
		key, ok := values[i].(string)
		if !ok {
			return nil, fmt.Errorf("dict keys must be strings")
		}
		dict[key] = values[i+1]
	}
	return dict, nil
}
//KAP : arrayClosest ( from adi90x/rancher-gen-rap)
func arrayClosest(values []string, input string) string {
	best := ""
	for _, v := range values {
		if strings.Contains(input, v) && len(v) > len(best) {
			best = v
		}
	}
	return best
}
//KAP : arrayFirst ( from adi90x/rancher-gen-rap)
func arrayFirst(input interface{}) interface{} {
	if input == nil {
		return nil
	}

	arr := reflect.ValueOf(input)

	if arr.Len() == 0 {
		return nil
	}

	return arr.Index(0).Interface()
}
//KAP : arrayLast ( from adi90x/rancher-gen-rap)
func arrayLast(input interface{}) interface{} {
	arr := reflect.ValueOf(input)
	return arr.Index(arr.Len() - 1).Interface()
}
//KAP : coalesce ( from adi90x/rancher-gen-rap)
func coalesce(input ...interface{}) interface{} {
	for _, v := range input {
		if v != nil {
			return v
		}
	}
	return nil
}
// KAP : dirList ( from adi90x/rancher-gen-rap)
func dirList(path string) ([]string, error) {
	names := []string{}
	files, err := ioutil.ReadDir(path)
	if err != nil {
		fmt.Printf("Template error: %v", err)
		return names, nil
	}
	for _, f := range files {
		names = append(names, f.Name())
	}
	return names, nil
}

//KAP : Combine  two string slice and remove duplicate
func concatenateUnique(slice1 []string, slice2 []string) []string {
    elements := append(slice1, slice2...)
    encountered := map[string]bool{}
    // Create a map of all unique elements.
    for v:= range elements {
	encountered[elements[v]] = true
    }
    // Place all keys from the map into a slice.
    result := []string{}
    for key, _ := range encountered {
	result = append(result, key)
    }
		sort.Strings(result)
    return result
}
//KAP :  when returns the trueValue when the condition is true and the falseValue otherwise
func when(condition bool, trueValue, falseValue interface{}) interface{} {
	if condition {
		return trueValue
	} else {
		return falseValue
	}
}
//KAP :  return a string with no "_", ",",";" to create a clean name
func formatClean(toclean string ) string {
	cleaned := strings.Replace(toclean,"_","",-1)
	cleaned = strings.Replace(cleaned,",","",-1)
	cleaned = strings.Replace(cleaned,";","",-1)
	cleaned = strings.Replace(cleaned," ","",-1)
	cleaned = strings.Replace(cleaned,".","",-1)
	cleaned = strings.Replace(cleaned,"-","",-1)
	return cleaned
}

//KAP : GroupbyMulti ( from jwilder/dockergen)

func groupByMulti(label string, sep string, in interface{}) (map[string][]interface{}, error) {
	m := make(map[string][]interface{})

	if in == nil {
		return m, fmt.Errorf("(groupByMulti) input is nil")
	}

	switch typed := in.(type) {
	case []corev1.Service:
		for _, s := range typed {
			value, ok := s.Annotations[label]
			if ok && len(value) > 0 {
				items := strings.Split(string(value), sep)
				for _, item := range items {
				m[item] = append(m[item], s)
				}
			}
		}
	case []corev1.Pod:
		for _, c := range typed {
			value, ok := c.Annotations[label]
			if ok && len(value) > 0 {
				items := strings.Split(string(value), sep)
				for _, item := range items {
				m[item] = append(m[item], c)
				}
			}
		}
	case []corev1.Node:
		for _, h := range typed {
			value, ok := h.Annotations[label]
			if ok && len(value) > 0 {
				items := strings.Split(string(value), sep)
				for _, item := range items {
				m[item] = append(m[item], h)
				}
			}
		}
	default:
		return m, fmt.Errorf("(groupByMulti) invalid input type %T", in)
	}

	return m, nil
}

//KAP: groupByMultiFilter => group by multi but filter on service name ( use to get containers with no service name and threat them as standalone containers)
func groupByMultiFilter(filter string, label string, sep string, in interface{}) (map[string][]interface{}, error) {
  return nil, fmt.Errorf("(groupByMultiFilter) not implemented")
}
// 	m := make(map[string][]interface{})

// 	if in == nil {
// 		return m, fmt.Errorf("(groupByMultiFilter) input is nil")
// 	}

// 	switch typed := in.(type) {
// 	case []corev1.Service:
// 		for _, s := range typed {
// 			value, ok := s.Annotations[label]
// 			if ok && len(value) > 0 && s.Name == filter {
// 				items := strings.Split(string(value), sep)
// 				for _, item := range items {
// 				m[item] = append(m[item], s)
// 				}
// 			}
// 		}
// 	case []corev1.Pod:
// 		for _, c := range typed {
// 			value, ok := c.Annotations[label]
// 			if ok && len(value) > 0 && c.Selector == filter {
// 				items := strings.Split(string(value), sep)
// 				for _, item := range items {
// 				m[item] = append(m[item], c)
// 				}
// 			}
// 		}
// 	case []corev1.Node:
// 		for _, h := range typed {
// 			value, ok := h.Annotations[label]
// 			if ok && len(value) > 0 {
// 				items := strings.Split(string(value), sep)
// 				for _, item := range items {
// 				m[item] = append(m[item], h)
// 				}
// 			}
// 		}
// 	default:
// 		return m, fmt.Errorf("(groupByMultiFilter) invalid input type %T", in)
// 	}

// 	return m, nil
// }

//KAP: filterHost => filter on Host name ( use to get containers on a specific host name )
func filterHost(filter string, inlist []interface{}) ([]interface{}, error) {
	if filter == string("*") {
	return inlist,nil
	}

	if inlist == nil {
		return inlist, fmt.Errorf("(filterHost) input is nil")
	}

	result := make([]interface{},0)
	var re = regexp.MustCompile(filter)

	for _, in := range inlist {
		if _, ok := in.(corev1.Pod); ok {
			if  re.Match([]byte(in.(corev1.Pod).Spec.NodeName)) {
    			result = append(result,in)
			}
		} else {
			result = append(result, in)
		}
	}
	return result, nil
}

//KAP: filterLabel => filter on a label name ( use to get containers that match a specific label )
func filterLabel(label string,filter string, inlist []interface{}) ([]interface{}, error) {
	if filter == string("*") {
	return inlist,nil
	}

	if inlist == nil {
		return inlist, fmt.Errorf("(filterLabel) input is nil")
	}

	result := make([]interface{},0)
	var re = regexp.MustCompile(filter)

	for _, in := range inlist {
		if _, ok := in.(corev1.Pod); ok {
			if  re.Match([]byte(in.(corev1.Pod).Annotations[label])) {
    			result = append(result,in)
			}
		}
		if _, ok := in.(corev1.Service); ok {
			if  re.Match([]byte(in.(corev1.Service).Annotations[label])) {
    			result = append(result,in)
			}
		}
	}

	return result, nil
}

func whereLabel(funcName string, in interface{}, label string, test func(string, bool) bool) ([]interface{}, error) {
	result := make([]interface{}, 0)
	if in == nil {
		return result, fmt.Errorf("(%s) input is nil", funcName)
	}
	if label == "" {
		return result, fmt.Errorf("(%s) label is empty", funcName)
	}

	switch typed := in.(type) {
	case []corev1.Service:
		for _, s := range typed {
			value, ok := s.Annotations[label]
			if test(value, ok) {
				result = append(result, s)
			}
		}
	case []corev1.Pod:
		for _, c := range typed {
			value, ok := c.Annotations[label]
			if test(value, ok) {
				result = append(result, c)
			}
		}
	case []corev1.Node:
		for _, s := range typed {
			value, ok := s.Annotations[label]
			if test(value, ok) {
				result = append(result, s)
			}
		}
	default:
		return result, fmt.Errorf("(%s) invalid input type %T", funcName, in)
	}

	return result, nil
}

// selects services or hosts from the input that have the given label
func whereLabelExists(label string, in interface{}) ([]interface{}, error) {
	return whereLabel("whereLabelExists", in, label, func(_ string, ok bool) bool {
		return ok
	})
}

// selects services or hosts from the input that have the given label and value
func whereLabelEquals(label, labelValue string, in interface{}) ([]interface{}, error) {
	return whereLabel("whereLabelEquals", in, label, func(value string, ok bool) bool {
		return ok && strings.EqualFold(value, labelValue)
	})
}

// selects services or hosts from the input that have the given label whose value matches the regex
func whereLabelMatches(label, pattern string, in interface{}) ([]interface{}, error) {
	rx, err := regexp.Compile(pattern)
	if err != nil {
		return nil, err
	}

	return whereLabel("whereLabelMatches", in, label, func(value string, ok bool) bool {
		return ok && rx.MatchString(value)
	})
}

//KAP: getAllLabelValue => get all the value for a given label
func getAllLabelValue(filter string,label string, sep string, in interface{}) ([]string, error) {
    m := make([]string,0)

	if in == nil {
		return m, fmt.Errorf("(getAllLabelValue) input is nil")
	}

	switch typed := in.(type) {
	case []corev1.Service:
		for _, s := range typed {
			value, ok := s.Annotations[label]
			if filter != string("*") {
			    if ok && len(value) > 0 && s.Name == filter {
				    items := strings.Split(string(value), sep)
				    for _, item := range items {
				    m = append(m, item)
				    }
		    	}
			} else
			{
			    if ok && len(value) > 0  {
				    items := strings.Split(string(value), sep)
				    for _, item := range items {
				    m = append(m, item)
				    }
		    	}
			}

		}
	case []corev1.Pod:
		for _, c := range typed {
			value, ok := c.Annotations[label]
			if filter != string("*") {
			    if ok && len(value) > 0 {
			    //if ok && len(value) > 0 && c.Service == filter {
				    items := strings.Split(string(value), sep)
				    for _, item := range items {
				    m = append(m, item)
				    }
		    	}
			} else
			{
			     if ok && len(value) > 0  {
				    items := strings.Split(string(value), sep)
				    for _, item := range items {
				    m = append(m, item)
				    }
		    	}
			}
		}
	case []corev1.Node:
		for _, h := range typed {
			value, ok := h.Annotations[label]
			if ok && len(value) > 0 {
				items := strings.Split(string(value), sep)
				for _, item := range items {
				m = append(m, item)
				}
			}
		}
	default:
		return m, fmt.Errorf("(getAllLabelValue) invalid input type %T", in)
	}

	return m, nil
}

//KAP  Adding Cell structure cf Rancher-gen-rap
//KAP Adding a storage place to enable modifying data inside template loop/conditional structure
type Cell struct{ v interface{} }

func NewCell(v ...interface{}) (*Cell, error) {
	switch len(v) {
	case 0:
		return new(Cell), nil
	case 1:
		return &Cell{v[0]}, nil
	default:
		return nil, fmt.Errorf("wrong number of args: want 0 or 1, got %v", len(v))
	}
}

func (c *Cell) Set(v interface{}) *Cell { c.v = v; return c }
func (c *Cell) Get() interface{}        { return c.v }

//KAP Add GetValue fonction
// KAP : Find in a map a specific value or return default value

func GetValue(mlabel map[string]string, filter string,def string) (string, error) {
	for k := range mlabel {
		if (k == filter) {
			return mlabel[k],nil
		}
	}
	return def,nil
}

func uniqueMap(prevmap map[string][]interface{}, maptoadd map[string][]interface{}) (map[string][]interface{}, error) {
	for k, v := range maptoadd {
     prevmap[k] = v
	}
	return prevmap,nil
}

func ExistValue(mlabel map[string]string, filter string) (bool, error) {
	for k := range mlabel {
		if (k == filter) {
			return true,nil
		}
	}
	return false,nil
}

//KAP: getPortsLen => get number of ports exposed by a service or pod
func getPortsLen(in interface{}) (int, error) {
	if in == nil {
		return 0, fmt.Errorf("(getPortsLen) input is nil")
	}
	switch typed := in.(type) {
		case corev1.Service:
			return len(typed.Spec.Ports), nil
		case corev1.Pod:
			firstcont := arrayFirst (typed.Spec.Containers).(corev1.Container)
			return len(firstcont.Ports), nil
		default:
			return 0, nil
	}

	return 0, fmt.Errorf("(getPortsLen) interface should be pod or service")
}

//KAP: getFirstPort => get first ports exposed by a service or pod
func getFirstPort(in interface{}, def int32) (int32, error) {
	if in == nil {
		return def, fmt.Errorf("(getFirstPort) input is nil")
	}
	switch typed := in.(type) {
		case corev1.Service:
			return arrayFirst(typed.Spec.Ports).(corev1.ServicePort).Port, nil
		case corev1.Pod:
			firstcont := arrayFirst (typed.Spec.Containers).(corev1.Container)
			return arrayFirst(firstcont.Ports).(corev1.ContainerPort).ContainerPort, nil
		default:
			return def, nil
	}

	return def, fmt.Errorf("(getFirstPort) interface should be pod or service")
}

func isOnline(in interface{}) (bool, error) {
	if in == nil {
		return false, fmt.Errorf("(isOnline) input is nil")
	}
	switch typed := in.(type) {
		case corev1.Service:
			return typed.Spec.Type == "ClusterIP" || typed.Spec.Type == "NodePort", nil
		case corev1.Pod:
			return typed.Status.Phase  == "Running", nil
		default:
			return false, nil
	}
	return false, fmt.Errorf("(isOnline) interface should be pod or service")
}

//KAP: getIP => get IP exposed by a service or pod
func getIP(in interface{}) (string, error) {
	if in == nil {
		return "", fmt.Errorf("(getIP) input is nil")
	}
	switch typed := in.(type) {
		case corev1.Service:
			return typed.Spec.ClusterIP, nil
		case corev1.Pod:
			return typed.Status.PodIP, nil
		default:
			return "", nil
	}

	return "", fmt.Errorf("(getIP) interface should be pod or service")
}

func aggregateMap(prevmap map[string][]interface{}, maptoadd map[string][]interface{}) (map[string][]interface{}, error) {
	for k, v := range maptoadd {
		for _,v2 := range v {
			prevmap[k] = append(prevmap[k], v2)
		}
	}
	return prevmap,nil
}
